import pandas as pd
from datetime import datetime
from modules.ibu_datacenter import IBUDataCenter
dc = IBUDataCenter()

season = "2018/2019"
races = []
for event in dc.list_events():
    for race in event.list_races():
        print event.name, race.name, race.date
        dt = datetime.strptime(race.date, "%d %b %H:%M")
        y0, y1 = [int(x) for x in season.split("/")]
        if dt.month < 6:
            dt = dt.replace(year=y1)
        else:
            dt = dt.replace(year=y0)
        races.append({
            "event": event.name,
            "race": race.name,
            "date": dt.strftime("%Y-%m-%d")
        })
file_path = "data/ibu/{}_race_calendar.csv".format(season.replace("/","-"))
pd.DataFrame(races).to_csv(file_path, encoding="utf-8")
print(u"Updated {}".format(file_path))
