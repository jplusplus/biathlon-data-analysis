# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException, ElementClickInterceptedException
import lxml.html
from time import sleep

BASE_URL = "https://biathlonresults.com/"


class IBUDataCenter():

    @property
    def driver(self):
        if not hasattr(self, "_driver"):
            self._driver = webdriver.Firefox()

        return self._driver


    @property
    def current_season(self):
        return self._season_elem.text

    def to_previous_season(self):
        self._season_elem.find_element_by_xpath("span[@click.trigger='Prev()']").click()
        sleep(1)
        return self.current_season

    def get_season(self, name):
        if self.current_season == name:
            return name
        for _ in range(0,10):
            _season = self.to_previous_season()
            if name == _season:
                return _season

        raise ValueError("No such season: {}".format(name))





    @property
    def _season_elem(self):
        return self.driver.find_element_by_xpath("//*[@currentitem.bind='SportData.CurrentSeason']")

    def list_events(self, season=None):
        events = []
        self.driver.get(BASE_URL)
        sleep(1)
        body = self.driver.find_element_by_tag_name('body')
        #body.send_keys(Keys.CONTROL, Keys.SUBTRACT)
        body.send_keys(Keys.LEFT_CONTROL, "-")

        if season is not None:
            self.get_season(season)

        for event_row in self.driver.find_elements_by_css_selector('div[au-target-id="109"]'):
            event = Event(event_row, self.driver)
            events.append(event)
        return events


    def get_event(self, name, season=None):
        for event in self.list_events(season=season):
            if name in event.name:
                return event
        raise ValueError("No such event: {}".format(name))



class Event():
    def __init__(self, elem, driver):
        self.elem = elem
        self.driver = driver
        self.cup_name = elem.find_element_by_xpath("div[1]/div/a/table/tbody/tr[1]/td[2]").text
        self.name = elem.find_element_by_xpath("div[1]/div/a/table/tbody/tr[2]/td[2]").text
        self.dates = " ".join([x.text for x in elem.find_elements_by_xpath("div[1]/div/a/table/tbody/tr/td[1]")])
        self.location = " ".join(self.name.split(" ")[:-1])
        self.country = self.name.split(" ")[-1]

    def list_races(self):
        self.elem.click()
        sleep(1)
        return [Race(x, self) for x in self.elem.find_elements_by_css_selector(".au-target.panel-collapse.collapse.in a")]

    def get_race(self, name):
        for race in self.list_races():
            if name in race.name:
                return race
        raise ValueError("No such race: {}".format(name))

    def load_analysis(self, race_name):
        race = self.get_race(race_name)
        race.load_analysis()


    def __repr__(self):
        return "<Event: {}: {}>".format(self.name, self.dates)

class Race():
    _is_open = False

    def __init__(self, elem, event):
        self.elem = elem
        self.event = event
        self.driver = event.driver
        assert isinstance(event, Event)
        self.name = elem.find_element_by_xpath("table/tbody/tr/td[2]").text.strip()
        self.date = elem.find_element_by_xpath("table/tbody/tr/td[1]").text.strip()

    def get_result_set(self, name):
        for result_set in self.result_sets:
            if name == result_set.name or name == result_set.name.lower():
                return result_set
        raise ValueError("No such result set: {}".format(name))


    def load_analysis(self):
        self._open()
        panel_elem = self.panel_elem

        analysis_link = panel_elem.find_element_by_xpath("//a[contains(text(), 'ANALYSIS')]")
        analysis_link.click()

        load_analysis_btn = panel_elem.find_element_by_xpath("//button[contains(text(), 'LOAD DETAILED ANALYSIS')]")
        load_analysis_btn.click()
        print("Start initing analysis for {}. This will take a few seconds.".format(self.name))
        for _ in range(0,20):
            if self.analysis_is_loaded:
                print("Analysis loaded")
                return
            else:
                sleep(0.5)
        raise Exception(u"Unable to load analysis for {}".format(self))

    def get_total_results(self):
        """Get the final, total results.

        """
        self.elem.click()
        sleep(0.5)
        # Click the result tab, in many cases redudant
        self.panel_elem.find_element_by_xpath("//a[contains(text(), 'RESULTS')]").click()
        # loaded with javascript, would be clener to check if list is actually ready
        sleep(1)

        result_list_html = self.panel_elem.find_element_by_xpath('//siwi-grid[@model.bind="$this.ResultsGrid"]/div/table')\
                                          .get_attribute("innerHTML")
        # parse with lxml for performance.
        root = lxml.html.fromstring(result_list_html)
        columns = [x.text_content() for x in root.xpath("thead/tr/th/span")]
        results = []
        for tr in root.xpath("tbody/tr"):
            values = [x.text_content().strip() for x in tr.xpath("td/span")]
            row = dict(zip(columns, values))
            results.append(row)

        return results



    def get_result_list(self, result_set, result_list):
        """Fetch a specific result list.

        :param result_set: name of result set, ie "Range an." or "COURSE AN."
        :param result_list: name of result list, ie "Shoot T. S1" or "Avg. Run Speed (km/h)"
        :returns: results as dict list
        """
        if "relay" in self.name.lower():
            raise NotImplementedError("Result parsing is not implemented for"
                                      "relays.")
        result_set = result_set.upper()

        ###
        # 1. Open replace content panel and select result list
        ###
        replace_content_panel = self._open_replace_content_panel()
        result_set_elem = replace_content_panel.find_element_by_xpath("//span[contains(text(), '{}')]/ancestor::div[@class='col-sm-2']".format(result_set))

        result_list_link = None
        result_list_name = None # ie "Range T. S1"
        for elem in result_set_elem.find_elements_by_css_selector(".list-group a"):
            if elem.text.lower() == result_list.lower():
                result_list_link = elem
                result_list_name = elem.text.strip()
                break
        if result_list_link is None:
            raise ValueError("{} is not a valid result list name under {}".format(result_list, result_set))

        result_list_link.click()
        sleep(1)

        ###
        # 2. Parse result list
        ###

        result_panel = self.driver.find_element_by_xpath("//span[@class='bladeTitle'][contains(text(), '{}')]/ancestor::blade-container".format(result_list_name))


        # 2.1 Click expand button ("<->") twice to show detailed results
        expand_btn = result_panel.find_element_by_xpath("//div[@click.trigger='blade.ChangeWidthIndex()']")
        expand_btn.click()
        expand_btn.click()

        # 2.2 Click the scroll down button ("↓") and parse html to end of table
        last_row = None
        results = []
        while True:
            for row in self._iter_result_list_rows(result_panel):
                if row not in results:
                    results.append(row)

            _last_row = results[-1]

            if _last_row == last_row:
                break
            else:
                last_row = _last_row
                # scroll down
                sleep(0.5)
                result_panel.find_element_by_xpath("//div[@click.trigger='blade.PageDown()']").click()
                print "scroll down"
                sleep(0.5)

        return results

    @staticmethod
    def _iter_result_list_rows(result_panel):
        """
        """
        # use lxml for faster parsing
        root = lxml.html.fromstring(result_panel.get_attribute("innerHTML"))
        for row_elem in root.xpath('//div[@click.delegate="RowClicked(row)"]'):
            row = dict([(cell.get("class").replace("au-target","").strip(), cell.text_content().strip()) for cell in row_elem.xpath("div")])
            row["colNat"] = row_elem.xpath("div[@class='colNat']/img")[0]\
                                    .get("src")\
                                    .replace("https://ibu.blob.core.windows.net/docs/flags/hd/", "")\
                                    .replace(".png","")
            yield row

    @property
    def result_sets(self):

        if not hasattr(self, "_result_sets"):
            self._result_sets = []
            replace_content_panel = self._open_replace_content_panel()
            for elem in replace_content_panel.find_elements_by_css_selector(".col-sm-2"):
                result_set = ResultSet(elem, self)
                if result_set.name in ["DATACENTER", "LIVE SCREENS"]:
                    continue
                else:
                    self._result_sets.append(result_set)
        return self._result_sets

    @property
    def panel_elem(self):
        self._open()
        panel_title = "{} | {}".format(self.event.location, self.name)
        return self.driver.find_element_by_xpath("//span[@class='bladeTitle'][contains(text(), '{}')]/ancestor::div[@class='bladeContainer au-target']".format(panel_title))


    @property
    def status(self):
        """Final|Canceled|Scheduled
        """
        return self.elem.find_element_by_css_selector(".normalStatus").text.strip()

    @property
    def analysis_is_loaded(self):
        try:
            self.panel_elem.find_element_by_xpath("//p[contains(text(), 'Analysis loaded')]")
            return True
        except NoSuchElementException:
            return False

    def _open(self):
        if not self._is_open:
            self.elem.click()
            sleep(0.5)
            self._is_open = True

    def _open_replace_content_panel(self):
        if not self.analysis_is_loaded:
            self.load_analysis()
        # click to open the panel
        try:
            self.panel_elem.find_elements_by_xpath("//div[@click.trigger='blade.ReplaceBlade()']")[0].click()
            sleep(0.5)
        except ElementClickInterceptedException:
            # making a somewhat unprecise assumption that panel is already open
            pass
        elem = self.driver.find_element_by_xpath("//span[contains(text(), 'REPLACE CONTENT')]/ancestor::div")

        return elem




    def __repr__(self):
        return "<Race: {}: {}>".format(self.name, self.date)


class ResultSet():
    def __init__(self, elem, race):
        self.race = race
        self.name = elem.find_element_by_xpath("div[1]/span[1]").text
        self.result_lists = []
        for a_elem in elem.find_elements_by_css_selector("a"):
            self.result_lists.append(a_elem.text)


    def __repr__(self):
        return "<ResultSet: {}, {}>".format(self.name, self.race.name)
