# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import os
from glob import glob
import urllib

DATA_DIR = "data/ibu/"  # for fetching data
TRANSLATIONS_CSV = "data/translations.csv" # for translating names
DROPBOX_PATH = u"https://www.dropbox.com/s/e6yw0wtposgc96i/{file_name}?dl=0"

CHART_DIR = u"/home/jens/Dropbox/Public/skidskytteanalys" # for storing charts
TABLES_DIR = u"/home/jens/Dropbox/Public/skidskytteanalys" # for stroring xlsx files


# Load data
def load_csv(file_path):
    file_name = (os.path.basename(file_path))
    # the file name is assumed to contain meta dta
    meta_data = zip(
        ["season", "event", "race", "result_set", "result_list"],
        file_name.replace(".csv","").split("|")
    )
    df = pd.read_csv(file_path, dtype=object)
    for key, value in meta_data:
        df[key] = value
    return df

def load_csv_files(file_paths):
    dfs = []
    for file_path in file_paths:
        dfs.append(load_csv(file_path))
    df = pd.concat(dfs).reset_index()
    df = df.rename(columns={
        "colShortName": "name",
        "colRank": "rank",
        "colBib": "bib",
        "colNat": "nat",
        "colResult": "value",
    })
    df["rank"] = pd.to_numeric(df["rank"].str.replace("*",""))
    df["sex"] = df["race"].str.split(" ").str[0].str.lower()
    return df

def get_df(sex, subset=None):
    file_paths = [x for x in glob(DATA_DIR + "*.csv") if "|{}".format(sex) in x]
    if subset:
        file_paths = [x for x in file_paths if subset in x]

    return load_csv_files(file_paths)

def parse_timedelta(x):
    if pd.isna(x):
        return x

    elif ":" in x:
        return pd.to_timedelta("00:" + x)
    else:
        return pd.to_timedelta("00:00:" + x)

def parse_relative_time(x):
    if pd.isna(x):
        return x
    elif x in ["DNS", "DNF", "LAP", "DSQ"]:
        return np.nan
    elif str(x).startswith("+"):
        return parse_timedelta(x)
    else:
        return pd.Timedelta(0)

def relative_to_absolute_time(s):
    """
    :param s: a series with strings expressing relative time (eg "+1:55.2"), assuming the first item contains the reference time (the best time)
    """
    best_time = parse_timedelta(s.iloc[0])
    return best_time + s.apply(parse_relative_time)



def get_ski_time(sex):
    """Get total ski time.
    """
    df = get_df(sex, "Course T. Total")
    #df["ski_time"] = df["value"].apply(parse_relative_time)
    df["ski_time"] = df.sort_values("rank").groupby(["event", "race", "sex"])["value"].transform(lambda x: relative_to_absolute_time(x))

    return df

def get_range_time(sex):
    df = get_df(sex, "Range T. Total")
    #df["range_time"] = df["value"].apply(parse_relative_time)
    df["range_time"] = df.sort_values("rank").groupby(["event", "race", "sex"])["value"].transform(lambda x: relative_to_absolute_time(x))
    return df

def get_misses(sex):
    df = get_df(sex, "Finish")
    df["misses"] = pd.to_numeric(df["colTotalPen"].str.replace("\n +","").str.replace("+",""))
    return df


def get_final_time(sex):
    df = get_df(sex, "Finish")
    df["final_time_abs"] = df.sort_values("rank").groupby(["event", "race", "sex"])["value"].transform(lambda x: relative_to_absolute_time(x))
    df["final_time_rel"] = df["value"].apply(parse_relative_time)
    return df


translations_df = pd.read_csv(TRANSLATIONS_CSV, encoding="utf-8").set_index("original")
def t(string):
    """Translate a string based on translations.csv.
    Returns unaltered string if not translation is provided
    """
    try:
        return translations_df.loc[string]["translated"].strip()
    except:
        return string

def dropbox_url(file_name):
    return DROPBOX_PATH.format(file_name=urllib.quote_plus(file_name.encode("utf-8")))


def save_chart(file_name, plt, fmt="svg"):
    full_file_name = file_name + u"." + fmt
    file_path = os.path.join(CHART_DIR, full_file_name)
    dropbox_path = dropbox_url(full_file_name)
    plt.savefig(file_path)
    #print u"Save chart to {}".format(file_path)
    print u"Chart url: {}".format(dropbox_path)

def save_table(file_name, df, fmt="xlsx"):
    full_file_name = file_name + u"." + fmt
    file_path = os.path.join(TABLES_DIR, full_file_name)
    dropbox_path = dropbox_url(full_file_name)
    df.to_excel(file_path, encoding="utf-8")
    #print u"Save table to {}".format(file_path)
    print u"Table url: {}".format(dropbox_path)
