import os
import pandas as pd
from modules.ibu_datacenter import IBUDataCenter
from time import sleep

OUTPUT_DIR = "data/ibu"
if not os.path.exists(OUTPUT_DIR):
    os.makedirs(OUTPUT_DIR)

dc = IBUDataCenter()
season = "2018/2019"
event_names = [event.name for event in dc.list_events(season=season)]
event_names = [ "Canmore"]
for event_name in event_names:
    sleep(3)
    event = dc.get_event(event_name)
    print(event)
    races = [race.name for race in event.list_races()]
    for race_name in races:
        if "Relay" in race_name:
            continue
        race = dc.get_event(event.name, season=season).get_race(race_name)
        print(race)
        if race.status != "FINAL":
            print("Ignoring race due to status '{}'".format(race.status))

        """
        sleep(2)
        total_res = race.get_total_results()
        file_name = "|".join([season.replace("/","-"), event.name, race.name, "total_results"]) + ".csv"
        file_path = os.path.join(OUTPUT_DIR, file_name)
        pd.DataFrame(total_res).to_csv(file_path, encoding="utf-8")
        print("Save to {}".format(file_path))
        """

        for result_set_name in ["INTERMEDIATES", "RANGE AN.", "COURSE AN."]:
            result_set = race.get_result_set(result_set_name)
            if result_set_name == "INTERMEDIATES":
                result_lists = ["Finish"]
            else:
                result_lists = result_set.result_lists
            for result_list in result_lists:
                res = race.get_result_list(result_set.name, result_list)
                file_name = "|".join([season.replace("/","-"), event.name, race.name, result_set.name, result_list.replace("/","")]) + ".csv"
                file_path = os.path.join(OUTPUT_DIR, file_name)
                pd.DataFrame(res).to_csv(file_path, encoding="utf-8")
                print("Save to {}".format(file_path))

dc.driver.close()
