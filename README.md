# Biathlon result scraping + analysis

This repo contains Python scripts for scraping results from the [IBU Data Center](https://biathlonresults.com/) and analyzing the results.

There is also a scraper for FIS results in `notebooks/FIS Scraper.ipynb`.


## Scripts

- `run.py`: Run IBU scraper. No CLI. Modify the script to change settings.


## Notebooks

- `notebooks/Preparation - Biathlon data.ipynb`: Prepares biatholn data for analysis.
- `notebooks/Analysis - Chance of winning a biathlon race.ipynb`: A statistical model for prediciting biatholon results.
- `notebooks/Analysis -  Age of CC medalists.ipynb`: An analysis of the age of cross country medalists over time.

The scraped data is not checked in to the repo. For the notebooks to work you first have to run scrapers.
